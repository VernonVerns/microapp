/*==================================================================
	`						Firebase Config`
===================================================================*/
var firebaseConfig = {
	apiKey: "AIzaSyD7xfB4SKgdul3GnSUlyHedoomvJ8FLcKk",
	authDomain: "linda-18e74.firebaseapp.com",
	databaseURL: "https://linda-18e74.firebaseio.com",
	projectId: "linda-18e74",
	storageBucket: "linda-18e74.appspot.com",
	messagingSenderId: "755773494408",
	appId: "1:755773494408:web:8c2f7edf38375f0aa4fb3d",
	measurementId: "G-6VNG2JBB9E"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const db = firebase.firestore();
const BusinessesColl = db.collection('Businesses');

$('#view_registration').on('click', function(){
	var modal = document.getElementById("register_biz");
	modal.style.display = "block";

	window.onclick = function(event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	}

	$('#closeRegisterBiz').on('click', function(){
		modal.style.display = "none";
	});
});

$('#form_registration').on('submit', function(e){
	e.preventDefault();
	var formObj = {};
	var error = false;
	var formEntries = JSON.parse(JSON.stringify(jQuery('#form_registration').serializeArray()));
	for (var i = 0; i < formEntries.length; i++) {
		var entry = formEntries[i];
		const value = entry.value;
		const name = entry.name;
		if (name == "name" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid business name.");
			error = true;
		}
		if (name == "description" && (value == null || value.length < 6)) {
			showSnackbar("Please Enter a valid business description.");
			error = true;
		}
		if (name == "addressLine1" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid Street.");
			error = true;
		}
		if (name == "addressCity" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid City.");
			error = true;
		}
		if (name == "email" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid email.");
			error = true;
		}
		if (name == "office_number" && (value == null || value.length < 3)) {
			showSnackbar("Please Enter a valid Office number.");
			error = true;
		}
		if (error) {
			$('[name="'+ name + '"]').focus();
			return;
		}
		formObj[name] = value;
	}
	BusinessesColl.doc().set(formObj).then(() =>{
		clearRegForm();
	}).catch((err) =>{
		showSnackbar(err.message);
	});
});

function clearRegForm(){
	$('#form_registration').find("input[type=text], textarea").val("");
}

function showSnackbar(message){
	console.log(message);
}

function increaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 1 : value;
  value++;
  document.getElementById('number').value = value;
}

function decreaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 1 : value;
  value < 2 ? value = 2 : '';
  value--;
  document.getElementById('number').value = value;
}